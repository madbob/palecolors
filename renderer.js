const fs = require('fs');
const os = require('os');
const jQuery = require('jquery');

let home_folder = os.homedir();
let config_folder = home_folder + '/.config/pale/color/';
let config_filename = 'colors.json';
let config_path = config_folder + config_filename;

function addPalettesList(data) {
    var home = jQuery('#home');
    var list = jQuery('#palettes-list');

    var row = list.find('.palette-row[data-attribute-index=' + data.index + ']').first();

    if (row.length == 0) {
        row = home.find('.models .palette-row').first().clone();
        row.attr('data-attribute-index', data.index);
        list.append(row);
    }

    var preview = row.find('.preview');
    preview.empty();

    for (c in data.colors) {
        preview.append('<div style="background-color: ' + data.colors[c] + '">&nbsp;</div>');
    }

    row.find('.palette-name-static').text(data.name);
}

// http://stackoverflow.com/questions/1740700/how-to-get-hex-color-value-rather-than-rgb-value
function rgb2hex(rgb) {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return ("#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3])).toUpperCase();
}

// https://jsfiddle.net/onury/Lcyrd1xy/
function invertColor(hex) {
    if (hex.indexOf('#') === 0)
        hex = hex.slice(1);

    if (hex.length === 3)
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    if (hex.length !== 6)
        throw new Error('Invalid HEX color.');

    var r = (255 - parseInt(hex.slice(0, 2), 16)).toString(16),
        g = (255 - parseInt(hex.slice(2, 4), 16)).toString(16),
        b = (255 - parseInt(hex.slice(4, 6), 16)).toString(16);
    return "#" + padZero(r) + padZero(g) + padZero(b);
}

function padZero(str, len) {
    len = len || 2;
    var zeros = new Array(len).join('0');
    return (zeros + str).slice(-len);
}

function appendRosterColor(color) {
    var composer = jQuery('#composer');
    var roster = composer.find('.palette-roster');
    var cell = composer.find('.models').find('.color-cell').first().clone();
    cell.find('.color-preview').css('background-color', color).css('color', invertColor(color));
    cell.find('.color-code').val(color);
    roster.append(cell);
}

function initComposer(data) {
    var composer = jQuery('#composer');

    if (data == null) {
        data = {
            index: jQuery('#palettes-list .palette-row').length,
            name: '',
            colors: [
                '#000000',
                '#FFFFFF',
                '#A7A9AC',
                '#ADD1D0',
                '#00716D',
                '#00526D'
            ]
        }

        composer.find('#delete-button').hide();
    }
    else {
        composer.find('#delete-button').show();
    }

    composer.find('.config').attr('data-attribute-index', data.index);
    composer.find('input.palette-name').val(data.name);
    composer.find('img.palette-source-image').hide();
    composer.find('.palette-roster').empty();

    for(var i in data.colors) {
        var color = data.colors[i];
        appendRosterColor(color);
    }
}

function dataByRow(row) {
    var data = {
        index: row.attr('data-attribute-index'),
        name: row.find('.palette-name-static').text(),
        colors: []
    };

    row.find('.preview div').each(function() {
        var p = jQuery(this);
        data.colors.push(rgb2hex(p.css('background-color')));
    });

    return data;
}

function saveFile() {
    var data = [];

    jQuery('#palettes-list .palette-row').each(function() {
        var t = jQuery(this);
        var c = dataByRow(t);
        data.push(c);
    });

    fs.writeFileSync(config_path, JSON.stringify(data));
}

function goToHome() {
    jQuery('#home').fadeIn();
    jQuery('#composer').fadeOut();
}

function goToComposer() {
    jQuery('#home').fadeOut();
    jQuery('#composer').fadeIn();
}

jQuery('#new-palette').click(function() {
    initComposer(null);
    goToComposer();
});

jQuery('#back-button').click(function() {
    goToHome();
});

jQuery('#save-button').click(function() {
    var composer = jQuery('#composer');
    var name = composer.find('.palette-name').val();
    if (name == '')
        name = 'unnamed new palette';

    var data = {
        index: composer.find('.config').attr('data-attribute-index'),
        name: name,
        colors: []
    }

    composer.find('.palette-roster .color-code').each(function() {
        data.colors.push(jQuery(this).val());
    });

    addPalettesList(data);
    saveFile();
    goToHome();
});

jQuery('#delete-button').click(function() {
    var composer = jQuery('#composer');
    var index = composer.find('.config').attr('data-attribute-index');
    jQuery('#palettes-list .palette-row[data-attribute-index=' + index + ']').remove();
    saveFile();
    goToHome();
});

jQuery('#add-button').click(function() {
    appendRosterColor('#FFFFFF');
});

jQuery('#palettes-list').on('click', '.palette-row', function() {
    var t = jQuery(this);
    var data = dataByRow(t);
    initComposer(data);
    goToComposer();
});

jQuery('#composer').on('keypress', '.color-code', function(e) {
    var k = e.keyCode;
    var valid_codes = [37, 39, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 97, 98, 99, 100, 101, 102, 35];
    if (valid_codes.includes(k) == false)
        e.preventDefault();

}).on('keyup', '.color-code', function() {
    var t = jQuery(this);
    var color = t.val().toUpperCase();
    if (color.indexOf('#') !== 0) {
        color = '#' + color;
        t.val(color);
    }
    t.css('color', invertColor(color)).closest('.color-cell').find('.color-preview').css('background-color', color);

}).on('blur', '.color-code', function() {
    var t = jQuery(this);
    var color = t.val().toUpperCase();
    t.val(color);
});

jQuery('#composer').on('click', '.fa-trash', function() {
    var t = jQuery(this);

    t.closest('.color-cell').fadeOut('fast', function() {
        this.remove();
    });
});

jQuery('#composer').hide();

if (fs.existsSync(config_path) == false) {
    fs.writeFileSync(config_path, '[]');
}

var saved = JSON.parse(fs.readFileSync(config_path, 'utf8'));
for(s in saved) {
    saved[s].index = s;
    addPalettesList(saved[s]);
}
